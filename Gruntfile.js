/* jshint node: true */

module.exports = function (grunt) {
    "use strict";

    // Node modules.
    var path = require("path");

    // Project configuration.
    grunt.initConfig({
        // pkg: grunt.file.readJSON("package.json"),
        jade: {
            static: {
                options: {
                    data: {
                        debug: false
                    }
                },
                files: {
                    "build/site/index.html": "src/jade/index.jade"
                }
            },
            template: {
                options: {
                    client: true,
                    namespace: "app.templates",
                    processName: function (filename) {
                        return path.basename(filename, ".jade");
                    }
                },
                files: {
                    "build/jade/templates.js": ["src/jade/templates/**/*.jade"]
                }
            }
        },
        stylus: {
            compile: {
                options: {},
                files: {
                    "build/site/index.css": "src/stylus/index.styl"
                }
            }
        },
        uglify: {
            index: {
                options: {
                    sourceMap: true
                },
                files: {
                    "build/site/index.min.js": [
                        "src/js/index.js"
                    ]
                }
            }
        },
        jshint: {
            options: {
                jshintrc: ".jshintrc"
            },
            all: ["Gruntfile.js", "src/js/**/*.js"]
        },
        jscs: {
            options: {
                config: ".jscsrc"
            },
            src: "src/js/**/*.js"
        },
        copy: {
            static: {
                expand: true,
                flatten: false,
                cwd: "src/static/",
                src: [
                    "**/*"
                ],
                dest: "build/site/"
            }
        },
        clean: ["build"]
    });

    // Load plugins.
    grunt.loadNpmTasks("grunt-contrib-jade");
    grunt.loadNpmTasks("grunt-contrib-stylus");
    grunt.loadNpmTasks("grunt-contrib-uglify");
    grunt.loadNpmTasks("grunt-contrib-jshint");
    grunt.loadNpmTasks("grunt-jscs");
    grunt.loadNpmTasks("grunt-contrib-copy");
    grunt.loadNpmTasks("grunt-contrib-clean");

    // Default task.
    grunt.registerTask("default", ["jade",
                                   "stylus",
                                   "uglify",
                                   "copy",
                                   "jshint",
                                   "jscs"]);
};
