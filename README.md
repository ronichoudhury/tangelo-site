# The Tangelo Website

This repository contains the source code for the Tangelo website, including
infrastructure for showing Tangelo demos.  The site is technically a [Tangelo
application](https://github.com/Kitware/tangelo) that serves an ``index.html``
at its root.  This allows the demos to show off features of Tangelo directly.

To build the site, first do ``npm install`` in the root of the repository,
followed by ``grunt``.  If all goes well, the Tangelo site will wind up in
``build/site``.

To serve the site, use the ``serve`` script in the root of the repository as
follows:

    $ ./serve

serves the site at ``localhost``, port 8080.

    $ ./serve 10000

serves it at ``localhost``, port 10000.  Finally,

    $ ./serve 0.0.0.0 8000

will serve it on ``localhost`` at port 8000, but the application will listen on
all interfaces, exposing the site on any domain names ``localhost`` is known as
on the local network.
